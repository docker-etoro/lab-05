# Docker Workshop
Lab 05: Building more complex images

---

## Overview

In this lab you will create a complex image to provide a single Dockerfile that can be configured for multiple environments (instead of creating a Dockerfile for each one).
Our application have two different configuration files (represented by html files), one for development and another for staging/production. In addition, each environment run in a different port (dev=5000, staging=80, production=443)


## Preparations

 - Create a new folder for the lab:
```
$ mkdir ~/lab-05
$ cd ~/lab-05
```


## Instructions

 - Create a index.html file for each configuration (dev,staging/production):
```
$ echo "<h1>Dev Environment</h1>" > ~/lab-05/dev.html
```
```
$ echo "<h1>Staging/Production Environment</h1>" > ~/lab-05/stg-prd.html
```

 - Create the Dockerfile for your image
```
nano ~/lab-05/Dockerfile
```

 - Set the content below
```
# Start from alpine version 3.4
FROM selaworkshops/alpine:3.4

# Install python
RUN apk-install python

# Set the container working directory
WORKDIR /app

# Configure the build-time variables
ARG configuration=dev
ARG port=5000

# Copy the "configuration" file
COPY $configuration.html /app/index.html

# Configure run-time variables
ENV port=$port

# Configure the container process
ENTRYPOINT python -m SimpleHTTPServer $port

# Expose the application
EXPOSE $port
```

 - Let's build an image for the development environment
```
$ docker build -t dev:latest .
```

 - Now create an image for staging/production environments
```
$ docker build --build-arg port=80 --build-arg configuration=stg-prd -t stg-prd:latest .
```

 - We are ready to run our containers, let's start with the development environment:
```
$ docker run -d -p 5000:5000 dev:latest
```

 - Browse to the development environment
```
http://<your-server-ip>:5000
```

 - run a container for the staging environment
```
docker run -d -p 80:80 stg-prd:latest
```

 - Browse to the staging environment
```
http://<your-server-ip>:80
```

 - Create a container for the production environment
```
docker run -d -e port=443 -p 443:443 stg-prd:latest
```

 - Browse to the production environment
```
http://<your-server-ip>:443
```


## Cleanup

 - Remove containers:
```
$ docker rm -f $(docker ps -a -q)
```

 - Remove images:
```
$ docker rmi $(docker images -q)
```

